<?php

namespace App\Models;
use CodeIgniter\Model;

class ApiRestfulCrudModel extends Model
{
    protected $table;
    protected $primaryKey;
    protected $allowedFields;

    public function setTable($table_name)
    {
       $this->table = $table_name;
    }

    public function setPrimaryKey($primary_key)
    {
       $this->primaryKey = $primary_key;
    }
    public function getPrimaryKey()
    {
       return $this->primaryKey;
    }
    public function setAllowedFields($allowed_fields)
    {
       $this->allowedFields = $allowed_fields;
    }
    public function getAllowedFields()
    {
       return $this->allowedFields;
    }
}

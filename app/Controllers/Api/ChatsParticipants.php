<?php
namespace App\Controllers\Api;

use App\Libraries\ApiRestfulCrud;

class ChatsParticipants extends ApiRestfulCrud
{
  protected $data = [    
    'idchatparticipant' => '',
    'idchat' => '',
    'wspidparticipant' => ''
  ];
  public function __construct()
  {
      parent::__construct();
      $this->getModel()->setTable("chatsparticipants");
      $this->getModel()->setPrimaryKey("idchatparticipant");
      $this->getModel()->setAllowedFields(array_keys($this->data));
      $this->setCRUD(false, false, false, false, false );
  }
  public function getChatParticipant()
  {
    return $this->getModel()->asObject()->where("idchat", $this->data['idchat'])->where("wspidparticipant", $this->data['wspidparticipant'])->first();
  }
}
?>

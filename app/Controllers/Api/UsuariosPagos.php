<?php
namespace App\Controllers\Api;

use App\Libraries\ApiRestfulCrud;

class UsuariosPagos extends ApiRestfulCrud
{
  protected $data = [
    'idusuariopago' => '',
    'idusuario' => '',
    'idpago' => '',
    'monto' => '',
    'fechapago' => '',
    'estado' => '',
    'detalle' => ''
  ];
  public function __construct()
  {
      parent::__construct();
      $this->getModel()->setTable("usuariospagos");
      $this->getModel()->setPrimaryKey("idusuariopago");
      $this->getModel()->setAllowedFields(array_keys($this->data));
      $this->setCRUD(false, false, false, false, false );
  }
}
?>

<?php
namespace App\Controllers\Api;

use App\Controllers\Crud\Chats;
use App\Libraries\ApiRestfulCrud;

class Whatsapi extends ApiRestfulCrud
{
  private $whatsapiuri = "http://whatsapi.avelo.com.ar";

  protected $lineaMGPS = '5491158175349';
  protected $lineaTaxiRosa = '5491158175349';

  public function __construct()
  {
      parent::__construct();
      $this->setCRUD(false, false, false, false, false );
  }

  /******************************************************************/
  /******************************************************************/
  /******************************************************************/

  private function _getChatById($idchat){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwchats");
    $auxW->getModel()->setPrimaryKey("idchat");
    return $auxW->getModel()->asObject()->find($idchat);
  }

  private function _getChats($idempresa){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwchats");
    $auxW->getModel()->setPrimaryKey("idchat");
    return $auxW->getModel()->asObject()->where("idempresa", $idempresa)->orderby("estado", "DESC")->orderby("orden", "ASC")->orderby("timestamp", "DESC")->findall();
  }

  private function _getChatsByUser($idempresa, $idusuario){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwchats");
    $auxW->getModel()->setPrimaryKey("idchat");
    return $auxW->getModel()->asObject()->where("idempresa", $idempresa)->where("idusuario", $idusuario)->orderby("timeusuario", "DESC")->findall();
  }

  private function _getChatByWspId($idempresa, $wspid){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwchats");
    $auxW->getModel()->setPrimaryKey("idchat");
    return $auxW->getModel()->asObject()->where("idempresa", $idempresa)->where("wspidchat", $wspid)->first();
  }

  private function _getMensajeById($idmensaje){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwmensajes");
    $auxW->getModel()->setPrimaryKey("idmensaje");
    return $auxW->getModel()->asObject()->find($idmensaje);
  }

  private function _getMensajeByWspId($wspid){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwmensajes");
    $auxW->getModel()->setPrimaryKey("idmensaje");
    return $auxW->getModel()->asObject()->where("wspidmensaje", $wspid)->orderby("idmensaje", "ASC")->findAll();
  }

  private function _getMensajes($idchat, $limit, $offset){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwmensajes");
    $auxW->getModel()->setPrimaryKey("idmensaje");
    return $auxW->getModel()->asObject()->where("idchat", $idchat)->limit($limit, $offset)->orderby("idmensaje", "DESC")->find();
  }

  private function _getMensajesNuevos($idchat, $idmensaje, $limit){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwmensajes");
    $auxW->getModel()->setPrimaryKey("idmensaje");
    return $auxW->getModel()->asObject()->where("idchat", $idchat)->where("idmensaje >", $idmensaje)->limit($limit)->orderby("idmensaje", "DESC")->find();
  }

  private function _getMensajesViejos($idchat, $idmensaje, $limit){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("vwmensajes");
    $auxW->getModel()->setPrimaryKey("idmensaje");
    return $auxW->getModel()->asObject()->where("idchat", $idchat)->where("idmensaje <", $idmensaje)->limit($limit)->orderby("idmensaje", "DESC")->find();
  }

  private function _getMultimedia($mediakey){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("multimedias");
    $auxW->getModel()->setPrimaryKey("idmultimedia");
    return $auxW->getModel()->asObject()->where("idmultimedia", $mediakey)->first();
  }

  private function _getLinea($wspidlinea){
    $auxW  = new Whatsapi;
    $auxW->getModel()->setTable("lineas");
    $auxW->getModel()->setPrimaryKey("idlinea");
    return $auxW->getModel()->asObject()->where("wspidlinea", $wspidlinea)->first();
  }

  private function _valsession($sessionhash)
  {
    $validate = new Validate();
    return $validate->valsession($sessionhash);
  }
  private function _unauthorized()
  {
    return $this->failUnauthorized("La session no es valida");
  }
  /******************************************************************/
  /******************************************************************/
  /******************************************************************/
  public function getChat($sessionhash, $idchat){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getChatById($idchat));
  }

  public function getChatByWspid($sessionhash, $wspid){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getChatByWspId($sess->idempresa, $wspid));
  }

  public function getChats($sessionhash){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getChats($sess->idempresa));
  }

  public function getRecentsChats($sessionhash){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getChatsByUser($sess->idempresa, $sess->idusuario));
  }

  public function getMensaje($sessionhash, $idmensaje){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getMensajeById($idmensaje));
  }

  public function getMensajeByWspId($sessionhash, $wspid){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getMensajeByWspId($wspid));
  }

  public function getMensajes($sessionhash, $idchat, $limit=25, $offset=0){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    $res = array_reverse($this->_getMensajes($idchat, $limit, $offset));
    return $this->respond($res);
  }

  public function getMensajesNuevos($sessionhash, $idchat, $idmensaje, $limit=25){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    $res = array_reverse($this->_getMensajesNuevos($idchat, $idmensaje, $limit));
    return $this->respond($res);
  }

  public function getMensajesViejos($sessionhash, $idchat, $idmensaje, $limit=25){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    $res = array_reverse($this->_getMensajesViejos($idchat, $idmensaje, $limit));
    return $this->respond($res);
  }

  public function getMultimedia($sessionhash, $mediakey){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    $file = $this->_getMultimedia($mediakey);
      // Build the headers to push out the file properly.
    header('Pragma: public');     // required
    header('Expires: 0');         // no cache
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    //header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
    header('Cache-Control: private',false);
    header('Content-Type: '.$file->contenttype);  // Add the mime type from Code igniter.
    //header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
    header('Content-Transfer-Encoding: binary');
    //header('Content-Length: '.filesize($path)); // provide file size
    header('Connection: close');
    echo base64_decode($file->content);
    exit();
  }

/******************************************************************/
/******************************************************************/
/******************************************************************/

  private function _sendMessageW($from, $to, $message){
    $data = [
      'from' => $from,
      'to' => $to,
      'message' => $message
    ];
    $client = \Config\Services::curlrequest([
      'baseURI' => $this->whatsapiuri,
    ]);
    $response = $client->request('POST', '/sendMessage', ['json' => $data]);
    return json_decode($response->getBody());
  }

  private function _getMessageW($from, $msgId){
    $data = [
      'from' => $from,
      'msgId' => $msgId
    ];
    $client = \Config\Services::curlrequest([
      'baseURI' => $this->whatsapiuri,
    ]);
    $response = $client->request('POST', '/getMessage', ['json' => $data]);
    return json_decode($response->getBody());
  }

  private function _getChatW($from, $chatId){
    $data = [
      'from' => $from,
      'chatId' => $chatId
    ];
    $client = \Config\Services::curlrequest([
      'baseURI' => $this->whatsapiuri,
    ]);
    $response = $client->request('POST', '/getChat', ['json' => $data]);
    return json_decode($response->getBody());
  }
  /******************************************************************/
  /******************************************************************/
  /******************************************************************/
  
  public function makeCodigoSeguridad($seed)
  {
    return str_pad(substr(hexdec(substr(md5($seed.microtime(true)),10,8)),-6), 6, '0', STR_PAD_LEFT);
  }
  
  public function sendCodigoSeguridad($to, $codigoseguridad)
  {
    $message="Nadie de Master Gps te va a solicitar este dato. !No lo compartas!\nTu código de seguridad es *$codigoseguridad*";
    $ret = $this->_sendMessageW($this->lineaMGPS, $to, $message);
  }

  public function sendNotificacionTaxiRosa($message)
  {
    $this->_sendMessageW($this->lineaTaxiRosa, '120363027024545701@g.us', $message);
  }

  public function sendMessage($sessionhash){
    $user = $this->_valsession($sessionhash);
    if (!$user) return $this->_unauthorized();
    $data = json_decode($this->request->getBody());
    $linea = (new Lineas())->getByIdEmpresa($user->idempresa);
    $data->from = explode("@", $linea->wspidlinea)[0];
    $data->message = "*".$user->nombre." ".$user->apellido."(Lic.".$user->licencia.")*:\n".$data->message;
    //log_message("alert", print_r($data, true));
    $ret = $this->_sendMessageW($data->from, $data->to, $data->message);
    $auxW = new Whatsapi();
    $auxW->_insertUsuariosMensajes($user->idusuario, $ret->msg->id->_serialized);
    
    log_message("alert", "enviado:".print_r($ret, true));
    if ($ret->msg->fromMe) {
      $chat = $this->_getChatByWspId($linea->idempresa, $ret->msg->to);
      $message=$user->nombre." ".$user->apellido."(Lic. ".$user->licencia."):\nresopndi a: *". $chat->name."*";
      log_message("alert", "notificacion:".$message);
      $this->sendNotificacionTaxiRosa($message);
    }
    return $this->respond($ret);
  }

  public function setEstado($sessionhash){
    $user = $this->_valsession($sessionhash);
    if (!$user) return $this->_unauthorized();
    $data = json_decode($this->request->getBody());
    
    //$linea = (new Lineas())->getByIdEmpresa($user->idempresa);
    //$data->from = explode("@", $linea->wspidlinea)[0];
    //$data->message = "*".$user->nombre." ".$user->apellido."(Lic.".$user->licencia.")*:\n\n".$data->message;
    //log_message("alert", print_r($data, true));
    $auxW = new Whatsapi();
    $auxW->getModel()->setTable("chats");
    $auxW->getModel()->setPrimaryKey("idchat");
    $auxW->getModel()->setAllowedFields([
      'idchat',
      'estado'
    ]);
    $ret = $auxW->getModel()->update($data->idchat, $data);
    return $this->respond($ret);
  }
  public function getMessageW($sessionhash, $from, $msgId){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getMessageW($from, $msgId));
  }

  public function getChatW($sessionhash, $from, $chatId){
    $sess = $this->_valsession($sessionhash);
    if (!$sess) return $this->_unauthorized();
    return $this->respond($this->_getChatW($from, $chatId));
  }

  public function receive(){
    $Whatsapi_data = json_decode(urldecode(substr($this->request->getBody(),5)));

    log_message("alert", "receive event:".print_r($Whatsapi_data->event ?? "",true));
    log_message("alert", "receive content:".print_r($Whatsapi_data->content ?? "",true));
    // aca me aseguro que la linea la tengo configurada
    $linea = $this->_getLinea($Whatsapi_data->whatsappid.'@c.us');
    if (!$linea)
    {
      return $this->failNotFound("No se encontro configuracion para la linea ".$Whatsapi_data->whatsappid.'@c.us');
    }

    if (in_array($Whatsapi_data->event, array('message_sent', 'message_recived')))
    {
      // aca me aseguro que el chat exista
      $chat = $this->_upsertChat($Whatsapi_data, $linea, ($Whatsapi_data->event=='message_sent')); //si 'message_sent' => leido true
      $this->_insertMensaje($Whatsapi_data, $chat);
      if ($Whatsapi_data->event == 'message_recived') 
      {
          $message="Hay un nuevo mensaje de: *".$chat->name."*";
          log_message("alert", "notificacion:".$message);
          $this->sendNotificacionTaxiRosa($message);
      }
    }
    if (in_array($Whatsapi_data->event, array('group_join', 'group_leave', 'group_update')))
    {
      $chat = $this->_upsertChat($Whatsapi_data, $linea, true);
      if($chat->isgroup)
      {
        $this->_updateParticipants($Whatsapi_data, $chat);
      }
    }
    return $this->respondCreated((object)["autoreply"=>'']);
  }

  public function tick()
  {
    //recibo evento y linea
    //upsert en lineastick (linea fehca hora)
    //leo empresascreditos (empresa a la que pertenece la linea)
    //leo lineas lineasticks entre las fechas del credito activo, de todas las lineas de la empresa
    //comparo y defino el estado actual. (aca hay que ver si lo hacemos con cada tick.. o al fin del dia)

  } 

  public function getTicks()
  {
    //recibo linea desde hasta agrupejo (h d m) 
    //leo lineas lineasticks entre las fechas de parametro, de todas las lineas de la empresa

  } 
/*
  public function getTicks()
  {
    //recibo linea desde hasta agrupejo (h d m) 
    //leo lineas lineasticks entre las fechas de parametro, de todas las lineas de la empresa

  } */

  public function getPlanes()
  {
    //recibo linea desde hasta agrupejo (h d m) 
    //leo lineas lineasticks entre las fechas de parametro, de todas las lineas de la empresa

  } 

  /******************************************************************/
  /******************************************************************/
  /******************************************************************/

  protected function _insertUsuariosMensajes($idusuario, $wspidmensaje)
  {
    $this->getModel()->setTable("usuariosmensajes");
    $this->getModel()->setPrimaryKey("idusuariomensaje");
    $this->getModel()->setAllowedFields([
      'idusuario',
      'wspidmensaje'
    ]);
    $data = [
      'idusuario' => $idusuario,
      'wspidmensaje' => $wspidmensaje
    ];
    $this->getModel()->insert($data, true);
  }

  protected function _upsertChat($Whatsapi_data, $linea, $leido)
  {
    $chat = $Whatsapi_data->content->chat;
    $auxW = new Whatsapi;
    $auxW->getModel()->setTable("chats");
    $auxW->getModel()->setPrimaryKey("idchat");
    $auxW->getModel()->setAllowedFields([
      'idchat',
      'idempresa',
      'idlinea',
      'wspidchat',
      'archived',
      'isgroup',
      'ismuted',
      'isreadonly',
      'muteexpiration',
      'name',
      'pinned',
      'timestamp',
      'unreadcount'
    ]);
    $data = [
      'idempresa' => $linea->idempresa,
      'idlinea' => $linea->idlinea,
      'wspidchat' => $chat->id->_serialized,
      'archived' => $chat->archived ?? false,
      'isgroup' => $chat->isGroup ?? false,
      'ismuted' => $chat->isMuted ?? false,
      'isreadonly' => $chat->isReadOnly ?? false,
      'muteexpiration' => ($chat->muteExpiration) ? date('Y-m-d H:i:s', $chat->muteExpiration) : null,
      'name' => $chat->name,
      'pinned' => $chat->pinned ?? false,
      'timestamp' => isset($chat->timestamp) ? date('Y-m-d H:i:s', $chat->timestamp) : date('Y-m-d H:i:s'),
      'unreadcount' => $chat->unreadCount
    ];
    if ($_chat = $this->_getChatByWspId($linea->idempresa, $chat->id->_serialized))
    {
      $idchat = $_chat->idchat;
      $auxW->getModel()->update($idchat, $data);
    }
    else
    {
      $idchat = $auxW->getModel()->insert($data, true);
    }
    $_chat = $this->_getChatById($idchat);
    if ($leido && $_chat->estado==1)
    {
      $auxW->getModel()->setAllowedFields([
        'idchat',
        'estado'
      ]);
      $data = [
        'estado' => 0
      ];
      $auxW->getModel()->update($idchat, $data);
    }
    if (!$leido && $_chat->estado==0)
    {
      $auxW->getModel()->setAllowedFields([
        'idchat',
        'estado',
        'fechaestado'
      ]);
      $data = [
        'estado' => 1,
        'fechaestado' => date('Y-m-d H:i:s')
      ];
      $auxW->getModel()->update($idchat, $data);
      //update esstado = 1, fechaesstado now();
    }
    return $_chat;
  }

  protected function _updateParticipants($Whatsapi_data, $chat)
  {
    $cp = new ChatsParticipants;
    foreach($Whatsapi_data->content->chat->groupMetadata->pastParticipants as $participant)
    {
      $cp->idchat = $chat->idchat;
      $cp->wspidparticipant = $participant->id->_serialized;
      if ($p=$cp->getChatParticipant()){
        $cp->idchatparticipant = $p->idchatparticipant;
        $cp->db_delete();
      }
    }  
    foreach($Whatsapi_data->content->chat->groupMetadata->participants as $participant)
    {
      $cp->idchat = $chat->idchat;
      $cp->wspidparticipant = $participant->id->_serialized;
      if (!$cp->getChatParticipant()){
        $cp->db_insert();
      }
    }
  }
  
  protected function _insertMensaje($Whatsapi_data, $chat)
  {
    $cont = $Whatsapi_data->content;
    $idmultimedia=null;
    if ($cont->hasMedia || isset($cont->location)){
      $wMultimedia = new Whatsapi();
      $wMultimedia->getModel()->setTable("multimedias");
      $wMultimedia->getModel()->setPrimaryKey("idmultimedia");
      $wMultimedia->getModel()->setAllowedFields([
        'mediakey',
        'contenttype',
        'content'
      ]);
      if ($cont->hasMedia) {
        $dataMM = [
          'mediakey' => $cont->mediaKey,
          'contenttype' => $Whatsapi_data->media->mimetype,
          'content' => $Whatsapi_data->media->data
        ];
      }
      if (isset($cont->location)) {
        $dataMM = [
          'mediakey' => 'location',
          'contenttype' => 'image/webp',
          'content' => $cont->body
        ];
        $cont->hasMedia = true;
        $cont->body = $cont->location->description;
      }
      $idmultimedia = $wMultimedia->getModel()->insert($dataMM, true);
    }
    $data = [
      'idchat' => $chat->idchat,
      'wspidmensaje' => $cont->id->_serialized,
      'ack' => $cont->ack,
      'author' => $cont->author ?? null,
      'body' => $cont->body,
      'broadcast' => $cont->broadcast ?? null,
      'devicetype' => $cont->deviceType,
      'forwardingscore' => $cont->forwardingScore,
      'sender' => $cont->from,
      'fromme' => $cont->fromMe,
      'hasmedia' => $cont->hasMedia ?? false,
      'hasquotedmsg' => $cont->hasQuotedMsg ?? false,
      'isforwarded' => $cont->isForwarded ?? false,
      'isstarred' => $cont->isStarred ?? false,
      'isstatus' => $cont->isStatus ?? false,
      'location' => isset($cont->location) ? $cont->location->longitude . ',' . $cont->location->latitude : null,
      'mediakey' => $cont->mediaKey ?? null,
      'timestamp' => date('Y-m-d H:i:s', $cont->timestamp),
      'recipient' => $cont->to,
      'type' => $cont->type,
      'wspidquotedmsg' => ($cont->hasQuotedMsg) ? $cont->quotedMsg->id->_serialized : null,
      'idmultimedia' => $idmultimedia
    ];
    //log_message("alert", "cont:".print_r($cont, true));
    //log_message("alert", "data:".print_r($data, true));
    $auxW = new Whatsapi;
    $auxW->getModel()->setTable("mensajes");
    $auxW->getModel()->setPrimaryKey("idmensaje");
    $auxW->getModel()->setAllowedFields([
      'idchat',
      'wspidmensaje',
      'ack',
      'author',
      'body',
      'broadcast',
      'devicetype',
      'forwardingscore',
      'sender',
      'fromme',
      'hasmedia',
      'hasquotedmsg',
      'isforwarded',
      'isstarred',
      'isstatus',
      'location',
      'mediakey',
      'timestamp',
      'recipient',
      'type',
      'wspidquotedmsg',
      'idmultimedia'
    ]);
    return $auxW->getModel()->insert($data, true);
  }
}

<?php
namespace App\Controllers\Api;

use App\Libraries\ApiRestfulCrud;

class Lineas extends ApiRestfulCrud
{
  protected $data = [
    'idlinea' => '',
    'idempresa' => '',
    'wsidlinea' => ''
  ];
  public function __construct()
  {
      parent::__construct();
      $this->getModel()->setTable("lineas");
      $this->getModel()->setPrimaryKey("idlinea");
      $this->getModel()->setAllowedFields(array_keys($this->data));
      $this->setCRUD(false, false, false, false, false );
  }
  public function getByIdEmpresa($idempresa)
  {    
    try {
      log_message("alert" , "en el get ".$this->getModel()->getPrimaryKey()."=".$this->data[$this->getModel()->getPrimaryKey()]);
      return $this->getModel()->asObject()->where("idempresa", $idempresa)->first();
    }
    catch(Exception $e){
      $this->lastError = $e;
      return false;
    }
  }
}
?>

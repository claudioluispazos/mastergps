<?php
namespace App\Controllers\Api;

use App\Libraries\ApiRestfulCrud;
use App\Libraries\MP;

class Validate extends ApiRestfulCrud
{
  public function __construct()
  {
      parent::__construct();
      $this->setCRUD(false, false, false, false, false );
  }

  /******************************************************************/
  /******************************************************************/
  /******************************************************************/

  private function _validar($codigo, $clave){
    $this->getModel()->setTable("usuarios");
    $this->getModel()->setPrimaryKey("idusuario");
    $ret = $this->getModel()->asObject()->where("email", $codigo)->where("clave", $clave)->first();
    if (!$ret) $ret = $this->getModel()->asObject()->where("usuario", $codigo)->where("clave", $clave)->first();
    return $ret;
  }

  private function _valsession($sessionhash){
    $this->getModel()->setTable("usuarios");
    $this->getModel()->setPrimaryKey("idusuario");
    return $this->getModel()->asObject()->where("sessionhash", $sessionhash)->where("estado", 1)->first();
  }

  /******************************************************************/
  /******************************************************************/
  /******************************************************************/

  public function login(){
    $data = json_decode($this->request->getBody());
    log_message("alert", "data".print_r($data,1));
    if (
      empty($data->codigo) ||
      empty($data->clave) ||
      empty($data->versionapp)
    )
    {
      log_message("alert", "faltan datos");
      return $this->failUnauthorized("Ingrese todos los datos requeridos");
    }
    if (version_compare($data->versionapp, "1.1", "<"))
    {
      log_message("alert", "version vieja");
      return $this->failUnauthorized("Debe Actualizar la Aplicacion");
    }
    $user = $this->_validar($data->codigo, $data->clave);
    if ($user)
    {
      if($user->estado==1)
      {
        //aca validar si esta en la lista de permitidos
        if (date("Y-m-d H:i:s") < $user->sessionexpires) 
        {
          /*$wp = new Whatsapi;
          $codigoseguridad = $wp->makeCodigoSeguridad($user->sessionhash);
          $wp->sendCodigoSeguridad($user->numerotelefono, $codigoseguridad);*/
          $codigoseguridad='';
          $ret = (object)[
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'sessionhash' => $user->sessionhash,
            'sessionexpires' => $user->sessionexpires,
            'codigoseguridad' => $codigoseguridad
          ];
          log_message("alert", "login ok");
          return $this->respond($ret);
        } 
        else 
        {
            $mp = new MP();
            $preference = $mp->getPreference($user);
            log_message("alert", "abono vencido");
            return $this->failValidationErrors([
              "message"=>"Abono vencido",
              "mplink" => $preference->init_point
            ]);
        }
      }
      else
      {
        log_message("alert", "usuario no habilitado");
        return $this->failUnauthorized("Usuario aun no habilitado");
      }
    } 
    else
    {
      log_message("alert", "clave erronea");
      return $this->failUnauthorized("El email o la clave proporcionadas no son validas");
    }
  }
  public function register(){
    $data = json_decode($this->request->getBody());
    log_message("alert", "data".print_r($data,1));
    if (
      empty($data->idempresa) ||
      empty($data->usuario) ||
      empty($data->email) ||
      empty($data->clave) ||
      empty($data->nombre) ||
      empty($data->apellido) ||
      empty($data->licencia) ||
      empty($data->numerotelefono) ||
      empty($data->tipodoc) ||
      empty($data->numdoc)
    )
    {
      log_message("alert", "faltan datos");
      return $this->failUnauthorized("Ingrese todos los datos requeridos");
    }
    $u = new Usuarios;
    $u->idempresa = $data->idempresa;
    $u->usuario = $data->usuario;
    $u->email = $data->email;
    $u->clave = $data->clave;
    $u->nombre = $data->nombre;
    $u->apellido = $data->apellido;
    $u->licencia = $data->licencia;
    $u->numerotelefono = $data->numerotelefono;
    $u->tipodoc = $data->tipodoc;
    $u->numdoc = $data->numdoc;

    if ($u->db_insert())
    {
      $user = $u->db_get();
      log_message("alert", "user".print_r($user,1));
      $wp = new Whatsapi;
      $codigoseguridad = $wp->makeCodigoSeguridad($user->sessionhash);
      $wp->sendCodigoSeguridad($user->numerotelefono, $codigoseguridad);
      $ret = (object)[
        'idusuario' => $user->idusuario,
        'nombre' => $user->nombre,
        'apellido' => $user->apellido,
        'sessionhash' => $user->sessionhash,
        'sessionexpires' => $user->sessionexpires,
        'codigoseguridad' => $codigoseguridad
      ];
      log_message("alert", "registro ok".print_r($ret,1));
      return $this->respond($ret);
    } 
    else 
    {
      log_message("alert", "error");
      return $this->failValidationErrors([
        "message"=>$u->lastError->getMessage()
      ]);
    }
  }
  public function activate(){
    $data = json_decode($this->request->getBody());
    log_message("alert", "data".print_r($data,1));
    if (
      empty($data->idusuario) ||
      empty($data->sessionhash) ||
      empty($data->codigoseguridad)
    )
    {
      log_message("alert", "faltan datos");
      return $this->failUnauthorized("Ingrese todos los datos requeridos");
    }    
    $u = new Usuarios;
    $u->idusuario = $data->idusuario;

    if ($user = $u->db_get())
    {
      if  ($user->estado==0 && $user->sessionhash == $data->sessionhash)
      {
        if ($u->activate())
        {
          $user = $u->db_get();
          $ret = (object)[
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'sessionhash' => $user->sessionhash,
            'sessionexpires' => $user->sessionexpires
          ];
          log_message("alert", "activado ".print_r($ret,1));
          return $this->respond($ret);
        }
      }
    }
    log_message("alert", "error");
    return $this->failValidationErrors([
      "message"=>"No se pudo activar el usuario"
    ]);

  }
  public function valsession($sessionhash)
  {
    return $this->_valsession($sessionhash);
  }

/******************************************************************/
/******************************************************************/
/******************************************************************/

}

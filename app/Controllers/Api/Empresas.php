<?php
namespace App\Controllers\Api;

use App\Libraries\ApiRestfulCrud;

class Empresas extends ApiRestfulCrud
{
  public function __construct()
  {
      parent::__construct();
      $this->getModel()->setTable("empresas");
      $this->getModel()->setPrimaryKey("idempresa");
      $this->getModel()->setAllowedFields([
        'idempresa',
        'codigo',
        'hostname'
      ]);
      $this->setCRUD(true, true, true, true, false );
  }
}
?>

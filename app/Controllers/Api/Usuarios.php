<?php
namespace App\Controllers\Api;

use App\Libraries\ApiRestfulCrud;

class Usuarios extends ApiRestfulCrud
{
  protected $data = [
    'idusuario' => '',
    'idempresa' => '',
    'usuario' => '',
    'email' => '',
    'clave' => '',
    'nombre' => '',
    'apellido' => '',
    'licencia' => '',
    'numerotelefono' => '',
    'tipodoc' => '',
    'numdoc' => ''
  ];
  public function __construct()
  {
      parent::__construct();
      $this->getModel()->setTable("usuarios");
      $this->getModel()->setPrimaryKey("idusuario");
      $this->getModel()->setAllowedFields(array_keys($this->data));
      $this->setCRUD(false, false, false, false, false );
  }
  public function updateSessionExpires($idusuario, $newdate)
  {
    $this->getModel()->setAllowedFields([
      'sessionexpires'
    ]);
    $data = [
      'sessionexpires' => $newdate
    ];
    $this->getModel()->update($idusuario, $data);
  }
  public function activate()
  {
    $this->getModel()->setAllowedFields([
      'estado'
    ]);
    $data = [
      'estado' => 1
    ];
    return $this->getModel()->update($this->idusuario, $data);    
  }
}
?>

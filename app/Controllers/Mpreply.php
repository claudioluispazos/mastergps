<?php

namespace App\Controllers;

use App\Controllers\Api\Usuarios;
use App\Controllers\Api\UsuariosPagos;
use App\Libraries\MP;
use DateInterval;
use DateTime;

class Mpreply extends BaseController
{
    public function index ()
    {
        echo "HOLA";
    }
    public function success()
    {
        echo "Hemos recibido su pago. Muchas Gracias";
        $this->log(__METHOD__);
        $this->log($this->request->getGet());
        /*$usu = new  Usuarios();
        $usu->updateAbono();*/
    }
    public function pending()
    {
        echo "Su pago se ecuentra pendiente.";
        $this->log(__METHOD__);
        $this->log($this->request->getGet());
    }
    public function failure()
    {
        echo "Su pago fue rechazado. Por favor intente nuevamente.";
        $this->log(__METHOD__);
        $this->log($this->request->getGet());
    }
    public function notification()
    {
        $this->log(__METHOD__);
        $this->log($this->request->getGet());
        $getData = $this->request->getGet();
        $mp = new MP();
/*
        if (isset($getData["topic"]) && $getData["topic"] == "merchant_order") {
            $notification = (new \MercadoPago\MerchantOrder())->get($getData["id"]);            
        }
*/
        if (isset($getData["type"]) && $getData["type"] == "payment") {
            $payment = (new \MercadoPago\Payment())->get($getData["data_id"]);
            log_message("alert", "payment:".print_r($payment ?? "",true));
            $usuarios = new Usuarios;
            $usuarios->idusuario=$payment->metadata->idusuario;
            $user = $usuarios->db_get();
        if ($payment->status=="approved")
            {
                if (date("Y-m-d H:i:s") > $user->sessionexpires) 
                {
                    $user->sessionexpires = date("Y-m-d H:i:s");
                }
                $usuarios->updateSessionExpires($user->idusuario, (new DateTime($user->sessionexpires))->add(new DateInterval("P30D"))->format("Y-m-d H:i:s"));
            }
            $usuariospagos = new UsuariosPagos;
            $usuariospagos->idusuario = $user->idusuario;
            $usuariospagos->idpago = $payment->id;
            $usuariospagos->monto = $payment->transaction_amount;
            $usuariospagos->fechapago = $payment->date_last_updated;
            $usuariospagos->estado = $payment->status;
            $usuariospagos->detalle = $payment->status_detail;
            $usuariospagos->db_insert();

        }
    }
    private function log($loque, $level="alert"){
        log_message($level, print_r($loque,1));
    }
}

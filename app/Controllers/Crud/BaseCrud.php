<?php namespace App\Controllers\Crud;

use App\Controllers\BaseController;
//use App\Libraries\GroceryCrud;

class BaseCrud extends BaseController
{
  protected $crud;
  protected $headerView;
  protected $contentView;
  public function __construct()
  {
    $this->crud = new myGroceryCrud();
    $this->emptyView = "crudHeader";
    $this->dataView = "crudData";
  }
  public function index() {
    $this->render(true);
  }
  public function render($empty=false) {
      if ($empty) echo view($this->emptyView);
      else echo view($this->dataView, (array) $this->crud->render());
  }
}
?>

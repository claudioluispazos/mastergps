<?php
namespace App\Libraries;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ApiRestfulCrudModel;
use Exception;

class ApiRestfulCrud extends ResourceController
{

  use ResponseTrait;
  private $apiModel;
  protected $data;
  private $CRUD = array(
    "C" => false, //0 no  1 si
    "RS" => false, //0 no  1 si
    "RA" => false, //0 no  1 si
    "U" => false, //0 no  1 si
    "D" => false  //0 no  1 si
  );
  public $lastError ;

  public function __construct()
  {
    $this->apiModel = new ApiRestfulCrudModel();
  }

  public function __set($name, $value)
  {
    $this->data[$name] = $value;
  }

  public function __get($name)
  {
    return $this->data[$name];
  }

  protected function setCRUD(bool $C, bool $RS, bool $RA, bool $U, bool $D)
  {
    $this->CRUD["C"] = $C;
    $this->CRUD["RS"] = $RS;
    $this->CRUD["RA"] = $RA;
    $this->CRUD["U"] = $U;
    $this->CRUD["D"] = $D;
  }

  protected function can($crud)
  {
    return $this->CRUD[$crud];
  }

  protected function getModel()
  {
      return $this->apiModel;
  }

  public function db_insert()
  {
    try {
      log_message("alert" , "antes del insert".print_r($this->data,1));
      $this->data[$this->getModel()->getPrimaryKey()] = $this->getModel()->insert($this->data, true);
      log_message("alert" , "despues del insert".print_r($this->data,1));
      return true;
    }
    catch(Exception $e){
      $this->lastError = $e;
      return false;
    }
  }

  public function db_update()
  {    
    try {
      $this->getModel()->update($this->data[$this->getModel()->getPrimaryKey()], $this->data);
      return true;
    }
    catch(Exception $e){
      $this->lastError = $e;
      return false;
    }
  }

  public function db_delete()
  {
    try {
      $this->getModel()->delete($this->data[$this->getModel()->getPrimaryKey()]);
      return true;
    }
    catch(Exception $e){
      $this->lastError = $e;
      return false;
    }
  }

  public function db_get()
  {    
    try {
      log_message("alert" , "en el get ".$this->getModel()->getPrimaryKey()."=".$this->data[$this->getModel()->getPrimaryKey()]);
      return $this->getModel()->asObject()->where($this->getModel()->getPrimaryKey(),$this->data[$this->getModel()->getPrimaryKey()])->first();
    }
    catch(Exception $e){
      $this->lastError = $e;
      return false;
    }
  }

  // C POST
  public function create()
  {
    if (!$this->can("C"))
    {
      return $this->failForbidden("Create fue deshabilitado para esta entidad");
    }
    $vars = json_decode(urldecode($this->request->getBody()));
    $data = array();
    foreach($this->apiModel->getAllowedFields() as $allowed_field)
    {
      $data[$allowed_field] = $vars->$allowed_field ?? null;
    }
    //print_r($data);
    //die();
    try {
      $this->apiModel->insert($data);
      $response = [
        'status'   => 201,
        'error'    => null,
        'messages' => [
            'success' => 'Operacion exitosa'
        ]
      ];
      return $this->respondCreated($response);
      }
    catch (Exception $e){
      return $this->fail($e->getMessage());  
    }
  }

  // R single GET
  public function show($id = null)
  {
    if (!$this->can("RS"))
    {
      return $this->failForbidden("Read Single fue deshabilitado para esta entidad");
    }
    $data = $this->apiModel->find($id);//where($this->apiModel->getPrimaryKey(), $id)->first();
    if($data){
        return $this->respond($data);
    }else{
        return $this->failNotFound('Customer does not exist.');
    }
  }

  // R all GET
  public function index()
  {
    if (!$this->can("RA"))
    {
      return $this->failForbidden("Real All fue deshabilitado para esta entidad");
    }
    $data = $this->apiModel->orderBy($this->apiModel->getPrimaryKey(), 'DESC')->findAll();
    return $this->respond($data);
  }

  // U update PUT
  public function update($id = null)
  {
    if (!$this->can("U"))
    {
      return $this->failForbidden("Update fue deshabilitado para esta entidad");
    }
    $data = array();
    $vars = json_decode(urldecode($this->request->getBody()));
    foreach($this->apiModel->getAllowedFields() as $allowed_field)
    {
      $data[$allowed_field] = $vars->$allowed_field;
    }
    $this->apiModel->update($id, $data);
    $response = [
      'status'   => 200,
      'error'    => null,
      'messages' => [
          'success' => 'Customer updated.'
      ]
    ];
    return $this->respond($response);
  }

  // D delete DELETE
  public function delete($id = null)
  {
    if (!$this->can("D"))
    {
      return $this->failForbidden("Delete fue deshabilitado para esta entidad");
    }
    $data = $this->apiModel->where($this->apiModel->getPrimaryKey(), $id)->delete($id);
    if($data){
        $this->apiModel->delete($id);
        $response = [
            'status'   => 200,
            'error'    => null,
            'messages' => [
                'success' => 'Customer deleted'
            ]
        ];
        return $this->respondDeleted($response);
    }else{
        return $this->failNotFound('Customer does not exist.');
    }
  }

}

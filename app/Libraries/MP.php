<?php
namespace App\Libraries;

use App\Controllers\BaseController;

class MP extends BaseController
{
    public function __construct()
    {
      // \MercadoPago\SDK::setAccessToken("APP_USR-5537404366470075-080606-0ae4c413ac0fc5922b67dbc1a7acc3d6-133997");
      \MercadoPago\SDK::setAccessToken("APP_USR-5537404366470075-080606-0ae4c413ac0fc5922b67dbc1a7acc3d6-133997");
    }

    /**
     * curl -X GET 'https://api.mercadopago.com/v1/identification_types' -H 'Authorization: Bearer TEST-5537404366470075-080606-6db3a404e5f2230f02ad1f6b7b08e536-133997'
     * [
     * {"id":"DNI","name":"DNI","type":"number","min_length":7,"max_length":8},
     * {"id":"CI","name":"Cédula","type":"number","min_length":1,"max_length":9},
     * {"id":"LC","name":"L.C.","type":"number","min_length":6,"max_length":7},
     * {"id":"LE","name":"L.E.","type":"number","min_length":6,"max_length":7},
     * {"id":"Otro","name":"Otro","type":"number","min_length":5,"max_length":20}
     * ]
     * 
     * curl -X GET 'https://api.mercadopago.com/v1/payment_methods' -H 'Authorization: Bearer TEST-5537404366470075-080606-6db3a404e5f2230f02ad1f6b7b08e536-133997'
     */
    public function getPreference($user)
    {
      $preference = new \MercadoPago\Preference();
      $preference->notification_url = site_url('Mpreply/notification');

      $item = new  \MercadoPago\Item();
      $item->id = "MTR";
      $item->title = "Mensajeria Taxi Rosa";
      $item->currency_id = "ARS";
      $item->description = "Abono Mensual";
      $item->quantity = 1;
      $item->unit_price = 700;
      $preference->items = [$item];

      $payer = new \MercadoPago\Payer();
      $payer->email = $user->email;
      $payer->identification = array(
          "type" => $user->tipodoc,
          "number" => $user->numdoc
      );
      $payer->first_name = $user->nombre;
      $payer->last_name = $user->apellido;
      $preference->payer = $payer;  

      $preference->metadata = $user;

      $preference->expires = true;
      $preference->save();

      return $preference;
        
    }
}
